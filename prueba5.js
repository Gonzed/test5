function myfunction(){
    var data = document.getElementById("data").value;
    var k = document.getElementById("k").value;
    var dataFinal
    var reg = new RegExp( `^.{0,${k-1}}[^ ](?= |$)` );
    try {
        if(k<1 || k >= 500){
            dataFinal = ""
        }else{
            dataFinal = data.match(reg);
        }

        console.log(dataFinal[0]);
    } catch (error) {
        console.log("Error: " + error)
    }
}